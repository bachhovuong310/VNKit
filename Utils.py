class TrieNode:
    def __init__(self):
        self.word = None
        self.value = None

        self.children = {}

    def insert(self, word, value=None):
        node = self
        for letter in word:
            if letter not in node.children:
                node.children[letter] = TrieNode()
            node = node.children[letter]
        node.word = word
        if value is not None:
            node.value = value

    def is_valid_word(self, word):
        node = self
        for letter in word:
            if letter not in node.children:
                return False
            else:
                node = node.children[letter]
        if node.word == word:
            return True
        return False

    def get_node_value(self, word):
        node = self
        for letter in word:
            if letter not in node.children:
                return None
            else:
                node = node.children[letter]
        if node.word == word:
            return node.value
        return None


def build_trie(words, values=None):
    trie = TrieNode()
    if values is not None:
        for i, w in enumerate(words):
            trie.insert(w, values[i])
    else:
        for w in words:
            trie.insert(w)

    return trie


def build_dictionary(values):
    key2value = sorted(set(values))
    value2key = dict((v, i) for i, v in enumerate(key2value))
    key2value = list(value2key)
    return key2value, value2key


def test_function(func, *args):  # with star
    print("{} --- {} --- {}".format(args, func.__name__, func(*args)))
