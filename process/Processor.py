import os


class Processor:
    def __init__(self, lang_helper):
        self.lang = lang_helper

    def convert_text2telex_file(self, input_path, output_path=None):
        print("\n(+) Convert file from text format to telex format : {}".format(input_path))
        if output_path is None:
            dir_name = os.path.dirname(input_path)
            base_name = os.path.basename(input_path)
            output_path = os.path.join(dir_name, 'telex_' + base_name)

        with open(input_path, 'r', encoding='utf8') as file_in:
            with open(output_path, 'w', encoding='utf8') as file_out:
                for line in file_in:
                    output_line = self.lang.any_to_telex(line)
                    file_out.write(output_line)
        print("Done !!! Save file to {}".format(output_path))

    def remove_diacritics_file(self, input_path, output_path=None):
        print("\n(+) Remove diacritics: {}".format(input_path))
        if output_path is None:
            dir_name = os.path.dirname(input_path)
            base_name = os.path.basename(input_path)
            output_path = os.path.join(dir_name, 'no_accent_' + base_name)

        with open(input_path, 'r', encoding='utf8') as file_in:
            with open(output_path, 'w', encoding='utf8') as file_out:
                for line in file_in:
                    output_line = self.lang.char.remove_diacritics(line)
                    file_out.write(output_line)
        print("Done !!! Save file to {}".format(output_path))

if __name__ == "__main__":
    from lang.Lang import LangHelper

    lang_helper = LangHelper()
    processor = Processor(lang_helper)

    input_path = "/home/user/Desktop/NewProjects/VnKit/data/clean/200MBtrain.txt"
    # input_path = "/home/user/Desktop/NewProjects/VnKit/data/clean/sample.txt"

    processor.convert_text2telex_file(input_path=input_path)
    # processor.remove_diacritics_file(input_path=input_path)
