from lang.Consonant import ConsonantHelper
from lang.Syllable import SyllableHelper
from lang.Tone import ToneHelper

from lang.Char import CharHelper
from lang.Rhyme import RhymeHelper


class LangHelper:
    def __init__(self):
        tone_helper = ToneHelper()
        char_helper = CharHelper(tone_helper=tone_helper)
        consonant_helper = ConsonantHelper()
        rhyme_helper = RhymeHelper(tone_helper=tone_helper, char_helper=char_helper)
        syllable_helper = SyllableHelper(tone_helper=tone_helper,
                                         char_helper=char_helper,
                                         consonant_helper=consonant_helper,
                                         rhyme_helper=rhyme_helper)

        self.tone = tone_helper
        self.char = char_helper
        self.consonant = consonant_helper
        self.rhyme = rhyme_helper
        self.syllable = syllable_helper

    def any_to_telex(self, text):
        new_text = ''
        for c in text:
            if self.char.is_char_or_number(c) or c == ' ':
                new_text += c
            else:
                new_text += " " + c + " "
        if new_text[-1] == ' ':
            new_text = new_text[0:-1]

        words = new_text.lower().split(' ')
        words_len = len(words)
        new_text = ''

        for i, w in enumerate(words):
            new_text += self.syllable.syllable_to_telex(w)

            if i < words_len - 1:
                new_text += ' '
        return new_text

if __name__ == "__main__":
    lang = LangHelper()

    from Utils import test_function

    print("(+) {}".format(lang.any_to_telex.__name__))
    test_function(lang.any_to_telex, "Chúc mừng năm mới")
    test_function(lang.any_to_telex,
                  "cả nhà ơi cho em hỏi nếu dữ liệu quá không cân bằng ví dụ như là phân lớp 2 lớp mà lớp 1 chỉ có 1/10 còn lớp 2 tới 9/10 thì làm thế nào để tránh việc nó overfit vô lớp thứ 2 ạ.")
    test_function(lang.any_to_telex, "Chúc mừng năm mới")
    test_function(lang.any_to_telex, "Chúc mừng năm mới")
