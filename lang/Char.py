class Char:
    NUMBERS = [u'0', u'1', u'2', u'3', u'4', u'5', u'6', u'7', u'8', u'9']
    BASE_CHARACTERS = [
        u'a', u'b', u'c', u'd', u'e', u'f', u'g', u'h', u'i', u'j', u'k', u'l', u'm', u'n',
        u'o', u'p', u'q', u'r', u's', u't', u'u', u'v', u'w', u'x', u'y', u'z'
    ]
    FULL_CHARACTERS = [
        u'a', u'b', u'c', u'd', u'e', u'f', u'g', u'h', u'i', u'j', u'k', u'l', u'm', u'n',
        u'o', u'p', u'q', u'r', u's', u't', u'u', u'v', u'w', u'x', u'y', u'z',
        u'à', u'á', u'â', u'ã', u'è', u'é', u'ê', u'ì', u'í', u'ò', u'ó',
        u'ô', u'õ', u'ù', u'ú', u'ý', u'ă', u'đ', u'ĩ', u'ũ', u'ơ', u'ư',
        u'ạ', u'ả', u'ấ', u'ầ', u'ẩ', u'ẫ', u'ậ', u'ắ', u'ằ', u'ẳ', u'ẵ', u'ặ',
        u'ẹ', u'ẻ', u'ẽ', u'ế', u'ề', u'ể', u'ễ', u'ệ', u'ỉ', u'ị', u'ọ', u'ỏ',
        u'ố', u'ồ', u'ổ', u'ỗ', u'ộ', u'ớ', u'ờ', u'ở', u'ỡ', u'ợ', u'ụ',
        u'ủ', u'ứ', u'ừ', u'ử', u'ữ', u'ự', u'ỳ', u'ỵ', u'ỷ', u'ỹ'
    ]

    ALL_VOWELS = [
        'a', 'à', 'á', 'ả', 'ã', 'ạ',
        'e', 'è', 'é', 'ẻ', 'ẽ', 'ẹ',
        'i', 'ì', 'í', 'ỉ', 'ĩ', 'ị',
        'o', 'ò', 'ó', 'ỏ', 'õ', 'ọ',
        'u', 'ù', 'ú', 'ủ', 'ũ', 'ụ',
        'y', 'ỳ', 'ý', 'ỷ', 'ỹ', 'ỵ',
        'â', 'ầ', 'ấ', 'ẩ', 'ẫ', 'ậ',
        'ê', 'ề', 'ế', 'ể', 'ễ', 'ệ',
        'ô', 'ồ', 'ố', 'ổ', 'ỗ', 'ộ',
        'ă', 'ằ', 'ắ', 'ẳ', 'ẵ', 'ặ',
        'ơ', 'ờ', 'ớ', 'ở', 'ỡ', 'ợ',
        'ư', 'ừ', 'ứ', 'ử', 'ữ', 'ự'
    ]

    TONES_VOWELS = {
        'a': u'aàáảãạ',
        'ă': u'ăằắẳẵặ',
        'â': u'âầấẩẫậ',
        'e': u'eèéẻẽẹ',
        'ê': u'êềếểễệ',
        'i': u'iìíỉĩị',
        'o': u'oòóỏõọ',
        'ô': u'ôồốổỗộ',
        'ơ': u'ơờớởỡợ',
        'u': u'uùúủũụ',
        'ư': u'ưừứửữự',
        'y': u'yỳýỷỹỵ'
    }

    VOWELS = [u'a', u'ă', u'â', u'e', u'ê', u'i', u'o', u'ô', u'ơ', u'u', u'ư', u'y']
    BASE_VOWELS = [u'a', u'a', u'a', u'e', u'e', u'i', u'o', u'o', u'o', 'u', 'u', 'y']  # tuong ung
    ADDITION_VOWELS = [u'', u'w', u'a', u'', u'e', u'', u'', u'o', u'w', u'', u'w', u'']


class CharHelper:
    def __init__(self, tone_helper):
        self.tone_helper = tone_helper

    def is_char(self, c):
        c = c.lower()
        return c in Char.FULL_CHARACTERS

    def is_number(self, c):
        return c in Char.NUMBERS

    def is_char_or_number(self, c):
        return self.is_char(c) or self.is_number(c)

    def is_vowel(self, c):
        c = c.lower()
        return c in Char.VOWELS

    def is_tone_vowel(self, c):
        c = c.lower()
        return c in Char.ALL_VOWELS

    # á -> a + 2 ; ố -> ô + 2
    def to_vowel_tone(self, c):
        if not self.is_tone_vowel(c):
            return None, None, False

        c = c.lower()

        for v in Char.TONES_VOWELS.keys():
            tone_vowels = Char.TONES_VOWELS[v]

            for t in range(len(tone_vowels)):
                if c == tone_vowels[t]:
                    return v, t, True

        return None, None, False

    # á -> a
    def to_base_vowel(self, c):
        vowel, t, done = self.to_vowel_tone(c)
        if not done:
            return None, False

        for i, v in enumerate(Char.VOWELS):
            if v == vowel:
                return Char.BASE_VOWELS[i], True

        return None, False

    def to_base_vowel_addition(self, v):
        for i, vowel in enumerate(Char.VOWELS):
            if vowel == v:
                return Char.BASE_VOWELS[i], Char.ADDITION_VOWELS[i], True
        return None, None, False

    # a + 1 -> à
    def add_vowel_tone(self, v, t):
        if not self.is_vowel(v) or not self.tone_helper.is_tone(t):
            return None, False
        if t == 0:
            return v, True

        v = v.lower()
        v = Char.TONES_VOWELS[v][t]
        return v, True

    def remove_diacritics(self, text):
        output = ''
        for c in text:
            if self.is_tone_vowel(c):
                v, done = self.to_base_vowel(c)
                if done:
                    output += v
            elif c == 'đ':
                output += 'd'
            else:
                output += c

        return output


if __name__ == "__main__":
    from Utils import test_function
    from lang.Tone import Tone, ToneHelper

    tone_helper = ToneHelper()
    char_helper = CharHelper(tone_helper=tone_helper)

    print("(+) {}".format(char_helper.add_vowel_tone.__name__))
    test_function(char_helper.add_vowel_tone, 'a', Tone.SAC)
    test_function(char_helper.add_vowel_tone, 'á', Tone.SAC)
