class Tone:
    NGANG = 0
    HUYEN = 1
    SAC = 2
    HOI = 3
    NGA = 4
    NANG = 5

    TONE_CHARS = ['', 'f', 's', 'r', 'x', 'j']
    ADDITIONS = ['', 'a', 'e', 'o', 'w', 'ww', 'd', 'da', 'de', 'do', 'dw', 'dww']
    ADDITION_TONES = [
        '', 'a', 'af', 'aj', 'ar', 'as', 'ax', 'd', 'da', 'daf', 'daj', 'dar', 'das', 'dax', 'de', 'def', 'dej', 'der',
        'des', 'dex', 'df', 'dj', 'do', 'dof', 'doj', 'dor', 'dos', 'dox', 'dr', 'ds', 'dw', 'dwf', 'dwj', 'dwr', 'dws',
        'dww', 'dwwf', 'dwwj', 'dwwr', 'dwws', 'dwwx', 'dwx', 'dx', 'e', 'ef', 'ej', 'er', 'es', 'ex', 'f', 'j', 'o',
        'of', 'oj', 'or', 'os', 'ox', 'r', 's', 'w', 'wf', 'wj', 'wr', 'ws', 'ww', 'wwf', 'wwj', 'wwr', 'wws', 'wwx',
        'wx', 'x'
    ]


class ToneHelper:
    def is_tone(self, t):
        if t is not None and isinstance(t, int) and 0 <= t and t <= 5:
            return True
        return False

    def is_tone_char(self, char):
        return char in Tone.TONE_CHARS

    def get_tone_char(self, t):
        if self.is_tone(t):
            return Tone.TONE_CHARS[t], True
        else:
            return None, False

    def tone_char2tone(self, tone_char):
        for index, char in enumerate(Tone.TONE_CHARS):
            if char == tone_char:
                return index, True
        return None, False

    def is_addition(self, a):
        for addition in Tone.ADDITIONS:
            if addition == a:
                return True
        return False


if __name__ == "__main__":
    from Utils import test_function

    tone_helper = ToneHelper()

    # Test
    print("(+) {}".format(tone_helper.is_tone.__name__))
    test_function(tone_helper.is_tone, -1)
    test_function(tone_helper.is_tone, 0)
    test_function(tone_helper.is_tone, 5)
    test_function(tone_helper.is_tone, 6)

    print("(+) {}".format(tone_helper.get_tone_char.__name__))
    test_function(tone_helper.get_tone_char, -1)
    test_function(tone_helper.get_tone_char, 0)
    test_function(tone_helper.get_tone_char, 5)
    test_function(tone_helper.get_tone_char, 6)
