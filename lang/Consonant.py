class Consonant:
    SINGLE_CONSONANTS = [
        u'b', u'c', u'd', u'đ', u'g', u'h', u'k', u'l', u'm', u'n',
        u'p', u'q', u'r', u's', u't', u'v', u'x'
    ]
    DI_CONSONANTS = [u'ch', u'gh', u'gi', u'kh', u'ng', u'nh', u'ph', u'th', u'tr']
    TRI_CONSONANTS = [u'ngh']
    CONSONANTS = SINGLE_CONSONANTS + DI_CONSONANTS + TRI_CONSONANTS


class ConsonantHelper:
    def __init__(self):
        pass

    def is_single_consonant(self, c):
        return c in Consonant.SINGLE_CONSONANTS

    def is_di_consonant(self, c):
        return c in Consonant.DI_CONSONANTS

    def is_tri_consonant(self, c):
        return c in Consonant.TRI_CONSONANTS

    def is_consonant(self, c):
        return self.is_single_consonant(c) or self.is_di_consonant(c) or self.is_tri_consonant(c)
