from Utils import build_trie
from lang.Char import Char


class Rhyme:
    # d r gi
    # c k q  g gh ng ngh du_dau vi_tri_them_dau(chi so cua nguyen am)
    # 0 0 0  0 0  0  0   0
    RULE_RHYMES = {
        # a
        u'a': '10010101',
        u'ai': '10010101',
        u'am': '10010101',
        u'an': '10010101',
        u'ang': '10010101',
        u'anh': '10010101',
        u'ao': '10010101',
        u'au': '10010101',
        u'ay': '10010101',
        u'ac': '10010100',
        u'ach': '10010100',
        u'ap': '10010100',
        u'at': '10010100',
        # # ă
        u'ăm': '10010101',
        u'ăn': '10010101',
        u'ăng': '10010101',
        u'ăc': '10010100',
        u'ăp': '10010100',
        u'ăt': '10010100',
        # â
        u'âm': '10010101',
        u'ân': '10010101',
        u'âng': '10010101',
        u'âu': '10010101',
        u'ây': '10010101',
        u'âc': '10010100',
        u'âp': '10010100',
        u'ât': '10010100',
        # e
        u'e': '01001011',
        u'em': '01001011',
        u'en': '01001011',
        u'eng': '01001011',
        u'eo': '01001011',
        u'ec': '01001010',
        u'ep': '01001010',
        u'et': '01001010',

        # ê
        u'ê': '01001011',
        u'êm': '01001011',
        u'ên': '01001011',
        u'ênh': '01001011',
        u'êu': '01001011',
        u'êch': '01001010',
        u'êp': '01001010',
        u'êt': '01001010',
        # i
        u'i': '01001011',
        u'ia': '01001011',
        u'im': '01001011',
        u'in': '01001011',
        u'inh': '01001011',
        u'iu': '01001011',
        # u'ic': '01001010', ?????????????????
        u'ich': '01001010',
        u'ip': '01001010',
        u'it': '01001010',
        # iê
        u'iêm': '01001011',
        u'iên': '01001011',
        u'iêng': '01001011',
        u'iêu': '01001011',
        u'iêc': '01001010',
        u'iêp': '01001010',
        u'iêt': '01001010',
        # o
        u'o': '10010101',
        # u'oa': '10010101',  # truong hop dac biet
        u'oi': '10010101',
        u'om': '10010101',
        u'on': '10010101',
        u'ong': '10010101',
        u'oc': '10010100',
        u'op': '10010100',
        u'ot': '10010100',
        # oa
        u'oa': '00110101',
        u'oai': '00110101',
        u'oan': '00110101',
        u'oam': '00110101',
        u'oang': '00110101',
        u'oanh': '00110101',
        u'oao': '00110101',
        u'oau': '00110101',
        u'oay': '00110101',
        u'oac': '00110100',
        u'oach': '00110100',
        u'oap': '00110100',
        u'oat': '00110100',
        # oă
        u'oăm': '00110101',
        u'oăn': '00110101',
        u'oăng': '00110101',
        u'oăc': '00110100',
        u'oăt': '00110100',
        u'oăp': '00110100',
        # oe
        u'oe': '00110101',
        u'oem': '00110101',
        u'oen': '00110101',
        # u'oeng': '00110101', ??????????????
        u'oeo': '00110101',
        u'oet': '00110100',
        # oo
        u'oong': '10010101',
        u'ooc': '10010100',
        # ô
        u'ô': '10010101',
        u'ôi': '10010101',
        u'ôm': '10010101',
        u'ôn': '10010101',
        u'ông': '10010101',
        u'ôc': '10010100',
        u'ôp': '10010100',
        u'ôt': '10010100',
        # ơ
        u'ơ': '10010101',
        u'ơi': '10010101',
        u'ơm': '10010101',
        u'ơn': '10010101',
        # u'ơng': '10010101', ?????????????????
        # u'ơc': '10010100', ??????????????????
        u'ơp': '10010100',
        u'ơt': '10010100',
        # u
        u'u': '10010101',
        u'ua': '10010101',
        u'ui': '10010101',
        u'um': '10010101',
        u'un': '10010101',
        u'ung': '10010101',
        u'uy': '10010101',
        u'uc': '10010100',
        u'up': '10010100',
        u'ut': '10010100',
        # uâ
        u'uân': '00110101',
        u'uâng': '00110101',
        u'uây': '00110101',
        u'uât': '00110100',
        # uê
        u'uê': '00110101',
        u'uêu': '00110101',
        u'uênh': '00110101',
        u'uên': '00110101',
        u'uêt': '00110100',
        u'uêch': '00110100',
        # uô
        u'uôi': '10010101',
        u'uôm': '10010101',
        u'uôn': '10010101',
        u'uông': '10010101',
        u'uôc': '10110100',
        u'uôp': '10010100',
        u'uôt': '10010100',
        # uơ
        u'uơ': '00110101',
        # uy
        u'uy': '00110101',
        u'uya': '00110101',
        u'uyn': '00110101',
        u'uynh': '00110101',
        u'uyu': '00110101',
        u'uych': '00110100',
        u'uyt': '00110100',
        # uyê
        u'uyên': '00110101',
        u'uyêt': '00110100',
        # ư
        u'ư': '10010101',
        u'ưa': '10010101',
        u'ưi': '10010101',
        u'ưm': '10010101',
        u'ưn': '10010101',
        u'ưng': '10010101',
        u'ưu': '10010101',
        u'ưc': '10010100',
        # u'ưp': '10010100', ?????????????
        u'ưt': '10010100',
        # ươ
        u'ươi': '10010101',
        u'ươm': '10010101',
        u'ươn': '10010101',
        u'ương': '10010101',
        u'ươu': '10010101',
        u'ươc': '10010100',
        u'ươp': '10010100',
        u'ươt': '10010100',
        # y
        u'y': '01000001',
        # yê
        u'yên': '00000001',
        u'yêm': '00000001',
        u'yêng': '00000001',
        u'yêu': '00000001',
        u'yêt': '00000000',
    }
    BASE_RHYMES = [
        'a', 'ac', 'ach', 'ai', 'am', 'an', 'ang', 'anh', 'ao', 'ap', 'at', 'au', 'ay',
        'e', 'ec', 'ech', 'em', 'en', 'eng', 'enh', 'eo', 'ep', 'et', 'eu',
        'i', 'ia', 'ich', 'iec', 'iem', 'ien', 'ieng', 'iep', 'iet', 'ieu', 'im', 'in', 'inh', 'ip', 'it', 'iu',
        'o', 'oa', 'oac', 'oach', 'oai', 'oam', 'oan', 'oang', 'oanh', 'oao', 'oap', 'oat', 'oau', 'oay', 'oc', 'oe',
        'oem',
        'oen', 'oeo', 'oet', 'oi', 'om', 'on', 'ong', 'ooc', 'oong', 'op', 'ot',
        'u', 'ua', 'uan', 'uang', 'uat', 'uay', 'uc', 'ue', 'uech', 'uen', 'uenh', 'uet', 'ueu', 'ui', 'um', 'un',
        'ung',
        'uo', 'uoc', 'uoi', 'uom', 'uon', 'uong', 'uop', 'uot',
        'uou', 'up', 'ut', 'uu', 'uy', 'uya', 'uych', 'uyen', 'uyet', 'uyn', 'uynh', 'uyt', 'uyu',
        'y', 'yem', 'yen', 'yeng', 'yet', 'yeu'
    ]
    MAX_RHYME_LEN = 5
    NUMBER_RHYMES = len(RULE_RHYMES.keys())


class RhymeHelper:
    def __init__(self, tone_helper, char_helper):
        self.tone_helper = tone_helper
        self.char_helper = char_helper
        self.rhyme_trie = build_trie(Rhyme.RULE_RHYMES.keys())
        self.base_rhyme_trie = build_trie(Rhyme.BASE_RHYMES)

    def is_rhyme(self, rhyme):
        rhyme = rhyme.lower()
        if rhyme == '' or len(rhyme) >= Rhyme.MAX_RHYME_LEN:
            return False
        return self.rhyme_trie.is_valid_word(rhyme)

    def is_base_rhyme(self, rhyme):
        rhyme = rhyme.lower()
        if rhyme == '' or len(rhyme) > Rhyme.MAX_RHYME_LEN:
            return False
        return self.base_rhyme_trie.is_valid_word(rhyme)

    def to_rhyme_tone(self, rhyme):
        if rhyme == '' or len(rhyme) >= Rhyme.MAX_RHYME_LEN:
            return None, None, False

        rhyme = rhyme.lower()
        __rhyme = list(rhyme)

        for i, c in enumerate(__rhyme):
            v, t, done = self.char_helper.to_vowel_tone(c)
            if done and t > 0:  # t=0 ko can phai thay doi ki tu tai vi tri i
                __rhyme[i] = v
                __rhyme = ''.join(__rhyme)

                if not self.is_rhyme(__rhyme):
                    return None, None, False

                return __rhyme, t, True

        __rhyme = ''.join(__rhyme)

        if not self.is_rhyme(__rhyme):
            return None, None, False

        return __rhyme, 0, True

    def add_rhyme_tone(self, rhyme, tone, is_q=False):
        if not self.is_rhyme(rhyme) or not self.tone_helper.is_tone(tone):
            return None, False

        rhyme = rhyme.lower()
        rhyme_len = len(rhyme)
        __rhyme = list(rhyme)

        nb_vowel = 0
        for c in __rhyme:
            if self.char_helper.is_vowel(c):
                nb_vowel += 1
            else:
                break
        # Xet tung truong hop ve so luong nguyen am co trong van
        if nb_vowel == 0:
            return rhyme, True

        if nb_vowel == 1:
            vowel = __rhyme[0]  # nguyen am se duoc them dau
            if self.char_helper.is_vowel(vowel):
                tv, done = self.char_helper.add_vowel_tone(vowel, tone)
                if done:
                    __rhyme[0] = tv
                    return ''.join(__rhyme), True

        # Neu xuat hien cac nguyen am ăâêôơư thi chac chan se danh dau o nhung nguyen am nay
        for i in reversed(range(rhyme_len)):
            c = __rhyme[i]
            if c in u'ăâêôơư':
                tv, done = self.char_helper.add_vowel_tone(c, tone)
                if done:
                    __rhyme[i] = tv
                    return ''.join(__rhyme), True

        if nb_vowel == 2:
            if not self.char_helper.is_vowel(__rhyme[-1]):
                toned_vowel = __rhyme[1]
                tv, done = self.char_helper.add_vowel_tone(toned_vowel, tone)
                if done:
                    __rhyme[1] = tv
                    return ''.join(__rhyme), True

            if is_q:
                toned_vowel = __rhyme[1]
                tv, done = self.char_helper.add_vowel_tone(toned_vowel, tone)
                if done:
                    __rhyme[1] = tv
                    return ''.join(__rhyme), True

            toned_vowel = __rhyme[0]
            tv, done = self.char_helper.add_vowel_tone(toned_vowel, tone)
            if done:
                __rhyme[0] = tv
                return ''.join(__rhyme), True

        if nb_vowel == 3:
            toned_vowel = __rhyme[1]
            tv, done = self.char_helper.add_vowel_tone(toned_vowel, tone)
            if done:
                __rhyme[1] = tv
                return ''.join(__rhyme), True

        # Phong truong hop sai
        return None, False

    def to_base_rhyme_addition(self, rhyme):
        rhyme = rhyme.lower()
        if not self.is_rhyme(rhyme):
            return None, None, False

        addition = u''
        rhyme = list(rhyme)
        vowels_len = len(Char.VOWELS)
        for i in range(len(rhyme)):
            for j in range(vowels_len):
                if rhyme[i] == Char.VOWELS[j]:
                    rhyme[i] = Char.BASE_VOWELS[j]
                    addition += Char.ADDITION_VOWELS[j]
                    break

        return ''.join(rhyme), addition, True

    def add_base_rhyme_addition(self, base_rhyme, addition):
        base_rhyme = base_rhyme.lower()
        if not self.is_base_rhyme(base_rhyme) or not self.tone_helper.is_addition(addition):
            return None, False

        if base_rhyme == 'uo' and addition == 'w':
            return 'uơ', True

        rhyme_len = len(base_rhyme)
        addition_len = len(addition)
        __base_rhyme = list(base_rhyme)
        # base_rhyme = 'uyen' addition = 'e'
        # uu + w -> uu
        # uo + w ->
        # count w
        nums_w = 0

        for i in range(addition_len):
            if addition[i] == u'w':
                nums_w += 1
        cur_index = 0

        # ao, eu
        # awow, ew
        for cur_add in range(addition_len):
            a = addition[cur_add]
            while cur_index < rhyme_len:
                cur_char = __base_rhyme[cur_index]
                if cur_char == a:
                    if a == 'a':
                        __base_rhyme[cur_index] = 'â'
                        break
                    elif a == 'o':
                        __base_rhyme[cur_index] = 'ô'
                        break
                    elif a == 'e':
                        __base_rhyme[cur_index] = 'ê'
                        break
                elif a == 'w':
                    if nums_w == 1:
                        # TH dac biet oa_w, ou_w
                        is_case_oa = cur_char == u'o' and cur_index + 1 < rhyme_len and __base_rhyme[
                                                                                            cur_index + 1] == u'a'
                        if is_case_oa:
                            __base_rhyme[cur_index + 1] = u'ă'
                            break

                        is_case_uo = cur_char == u'u' and cur_index + 1 < rhyme_len and __base_rhyme[
                                                                                            cur_index + 1] == u'o'
                        if is_case_uo:
                            __base_rhyme[cur_index + 1] = u'ơ'
                            break
                            # is_case_ua = cur_char == u'u' and cur_index + 1 < rhyme_len and __base_rhyme[cur_index + 1] == u'a'
                            # if is_case_ua:
                            #     __base_rhyme[cur_index + 1] = u'ă'
                            #     break
                            # if is_case_oa or is_case_uo or is_case_ua:
                            #     output += current_vowel
                            #     index_vowels += 1
                            #     current_vowel = vowels[index_vowels]

                    if cur_char == 'a':
                        __base_rhyme[cur_index] = 'ă'
                        break
                    elif cur_char == 'o':
                        __base_rhyme[cur_index] = 'ơ'
                        break
                    elif cur_char == 'u':
                        __base_rhyme[cur_index] = 'ư'
                        break

                cur_index += 1

        rhyme = ''.join(__base_rhyme)

        if not self.is_rhyme(rhyme):
            return None, False
        return rhyme, True


if __name__ == "__main__":
    from  Utils import test_function
    from lang.Tone import ToneHelper
    from lang.Char import CharHelper

    tone_helper = ToneHelper()
    char_helper = CharHelper(tone_helper=tone_helper)
    rhyme_helper = RhymeHelper(tone_helper=tone_helper, char_helper=char_helper)

    print("(+) {}".format(rhyme_helper.is_rhyme.__name__))
    test_function(rhyme_helper.is_rhyme, 'a')
    test_function(rhyme_helper.is_rhyme, 'á')
    test_function(rhyme_helper.is_rhyme, 'ch')
    test_function(rhyme_helper.is_rhyme, 'uyên')
    print()

    print("(+) {}".format(rhyme_helper.to_rhyme_tone.__name__))
    test_function(rhyme_helper.to_rhyme_tone, 'hải')
    test_function(rhyme_helper.to_rhyme_tone, 'ải')
    test_function(rhyme_helper.to_rhyme_tone, 'ướt')
    test_function(rhyme_helper.to_rhyme_tone, 'ƯỚt')
    test_function(rhyme_helper.to_rhyme_tone, 'òa')
    test_function(rhyme_helper.to_rhyme_tone, 'bình')
    print()

    print("(+) {}".format(rhyme_helper.to_base_rhyme_addition.__name__))
    test_function(rhyme_helper.to_base_rhyme_addition, 'hải')
    test_function(rhyme_helper.to_base_rhyme_addition, 'ải')
    test_function(rhyme_helper.to_base_rhyme_addition, 'ướt')
    test_function(rhyme_helper.to_base_rhyme_addition, 'ƯỚt')
    test_function(rhyme_helper.to_base_rhyme_addition, 'òa')
    test_function(rhyme_helper.to_base_rhyme_addition, 'bình')
    test_function(rhyme_helper.to_base_rhyme_addition, "uông")
    test_function(rhyme_helper.to_base_rhyme_addition, "ươn")
    test_function(rhyme_helper.to_base_rhyme_addition, "ưng")
    print()

    print(rhyme_helper.add_base_rhyme_addition.__name__)
    test_function(rhyme_helper.add_base_rhyme_addition, "yen", "e")
    test_function(rhyme_helper.add_base_rhyme_addition, "yen", "ee")
    test_function(rhyme_helper.add_base_rhyme_addition, "yen", "es")
    test_function(rhyme_helper.add_base_rhyme_addition, "uong", "ww")
    test_function(rhyme_helper.add_base_rhyme_addition, "uong", "w")
    test_function(rhyme_helper.add_base_rhyme_addition, "uo", "w")
    test_function(rhyme_helper.add_base_rhyme_addition, "uong", "ws")
    test_function(rhyme_helper.add_base_rhyme_addition, "uong", "o")
    print()
