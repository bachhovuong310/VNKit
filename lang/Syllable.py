from lang.Consonant import Consonant

from Utils import build_trie
from lang.Char import Char
from lang.Rhyme import Rhyme


class Syllable:
    MAX_SYLLABLE_LEN = 7


class SyllableHelper:
    def __init__(self, tone_helper, char_helper, consonant_helper, rhyme_helper):
        self.tone_helper = tone_helper
        self.char_helper = char_helper
        self.consonant_helper = consonant_helper
        self.rhyme_helper = rhyme_helper
        self.syllables = self.find_all_vn_syllables()
        base_syllables = [self.char_helper.remove_diacritics(syllable) for syllable in self.syllables]
        self.base_syllables = base_syllables

        self.syllable_trie = build_trie(self.syllables)
        self.base_syllable_trie = build_trie(self.base_syllables)

        self.telex_syllables = [self.syllable_to_telex(s) for s in self.syllables]
        self.telex_syllable_trie = build_trie(self.telex_syllables)

    def find_all_vn_syllables(self):
        syllables = []
        for rhyme in sorted(Rhyme.RULE_RHYMES.keys()):
            rule = Rhyme.RULE_RHYMES[rhyme]
            is_full_tone = rule[-1] == '1'
            if is_full_tone:
                tones = [0, 1, 2, 3, 4, 5]  # full
            else:
                tones = [2, 5]  # sac, nang

            consonants = []
            consonants.append('')
            consonants.extend(Consonant.CONSONANTS)

            if rule[0] == '0':
                consonants.remove('c')
            if rule[1] == '0':
                consonants.remove('k')
            if rule[2] == '0':
                consonants.remove('q')
            if rule[3] == '0':
                consonants.remove('g')
            if rule[4] == '0':
                consonants.remove('gh')
            if rule[5] == '0':
                consonants.remove('ng')
            if rule[6] == '0':
                consonants.remove('ngh')

            for c in consonants:
                for t in tones:
                    syllable, done = self.add_consonant_rhyme_tone(c, rhyme, t)
                    if done:
                        syllables.append(syllable)
        syllables = sorted(syllables)
        # with open('syllables.txt', 'w', encoding='utf8') as f:
        #     for s in syllables:
        #         f.write(s + '\n')

        return syllables

    def add_consonant_rhyme_tone(self, consonant, rhyme, tone):
        if not self.consonant_helper.is_consonant(consonant) \
                or not self.rhyme_helper.is_rhyme(rhyme) \
                or not self.tone_helper.is_tone(tone):
            return None, False

        is_q = consonant == 'q'
        is_gi = consonant == 'gi'

        if is_gi:
            if rhyme[0] == 'i':
                consonant = 'g'
        # tone rhyme
        rhyme, done = self.rhyme_helper.add_rhyme_tone(rhyme, tone, is_q)
        if not done:
            return None, False

        if is_q:
            rhyme = list(rhyme)
            if rhyme[0] == 'o':
                rhyme[0] = 'u'
            elif rhyme[0] != 'u':
                consonant = 'qu'
            rhyme = ''.join(rhyme)

        return consonant + rhyme, True

    def syllable_to_telex(self, syllable):
        output = ''
        addition = ''
        tone = ''
        for c in syllable:
            if not self.char_helper.is_tone_vowel(c):
                if c == 'đ':
                    output += 'd'
                    addition = 'd' + addition
                else:
                    output += c
            else:
                v, t, done = self.char_helper.to_vowel_tone(c)
                if done:
                    if tone == '':
                        t, done = self.tone_helper.get_tone_char(t)
                        if done:
                            tone = t

                    bv, a, done = self.char_helper.to_base_vowel_addition(v)
                    if done:
                        output += bv
                        addition += a

        output += addition + tone
        return output

    def is_syllable(self, s):
        s = s.lower()
        syllable_len = len(s)
        if s is None or syllable_len == 0 or syllable_len > Syllable.MAX_SYLLABLE_LEN:
            return False
        return self.syllable_trie.is_valid_word(s)

    def is_base_syllable(self, s):
        s = s.lower()
        syllable_len = len(s)
        if s is None or syllable_len == 0 or syllable_len > Syllable.MAX_SYLLABLE_LEN:
            return False
        return self.base_syllable_trie.is_valid_word(s)

    def is_telex_syllable(self, s):
        s = s.lower()
        syllable_len = len(s)
        if s is None or syllable_len == 0:
            return False
        return self.telex_syllable_trie.is_valid_word(s)

    def to_consonant_rhyme_tone(self, vn_syllable):
        if not self.is_syllable(vn_syllable):
            return None, None, None, False

        start_rhyme_index = 0
        for i, c in enumerate(vn_syllable):
            if self.char_helper.is_tone_vowel(c):
                start_rhyme_index = i
                break

        consonant = vn_syllable[0:start_rhyme_index]
        rhyme = vn_syllable[start_rhyme_index:]

        if consonant == u'g':
            if len(rhyme) >= 2:
                if rhyme[0] == 'i':
                    if self.char_helper.is_tone_vowel(rhyme[1]):
                        rhyme = rhyme[1:]
                        consonant += 'i'
                    else:
                        consonant += 'i'

        elif consonant == u'q':
            if len(rhyme) >= 2:
                if rhyme[0] == u'u' and rhyme[1] in Char.TONES_VOWELS['a'] + Char.TONES_VOWELS['ă'] + Char.TONES_VOWELS[
                    'e']:
                    rhyme = list(rhyme)
                    rhyme[0] = u'o'
                    rhyme = ''.join(rhyme)

        rhyme, tone, done = self.rhyme_helper.to_rhyme_tone(rhyme)
        if done:
            return consonant, rhyme, tone, True

        return None, None, None, False

    def to_consonant_base_rhyme_addition_tone(self, syllable):
        if not self.is_syllable(syllable):
            return None, None, None, None, False
        c, r, t, done = self.to_consonant_rhyme_tone(syllable)
        if not done:
            return None, None, None, None, False

        br, a, done = self.rhyme_helper.to_base_rhyme_addition(r)
        if not done:
            return None, None, None, None, False

        if c == 'đ':
            c = 'd'
            a = 'd' + a

        t, done = self.tone_helper.get_tone_char(t)
        if not done:
            return None, None, None, None, False

        return c, br, a, t, True

    def add_consonant_base_rhyme_addition_tone(self, c, br, a, t):
        if not self.consonant_helper.is_consonant(c) \
                or not self.tone_helper.is_tone(t) \
                or not self.rhyme_helper.is_base_rhyme(br) \
                or not self.tone_helper.is_addition(a):
            return None, False

        if c != '' and c == 'd':
            if a != '' and a[0] == 'd':
                c = 'đ'
                a = a[1:]

        r, done = self.rhyme_helper.add_base_rhyme_addition(br, a)
        if not done:
            return None, False

        return self.add_consonant_rhyme_tone(c, r, t)

    def vn_to_telex(self, vn_syllable):
        if not self.is_syllable(vn_syllable):
            return None, False

        c, r, t, done = self.to_consonant_rhyme_tone(vn_syllable)
        if not done:
            return None, False

        base_r, a, done = self.rhyme_helper.to_base_rhyme_addition(r)
        if not done:
            return None, False

        if c == 'gi' and base_r[0] == 'i':
            c = 'g'
        elif c == 'đ':
            c = 'd'
            a = 'd' + a
        elif c == 'q':
            if len(base_r) >= 2:
                base_r = list(base_r)
                if base_r[0] == 'o' and base_r[1] in 'ae':
                    base_r[0] = 'u'
                base_r = ''.join(base_r)

        tone, done = self.tone_helper.get_tone_char(t)
        if not done:
            return None, False

        return c + base_r + a + tone, True

    def telex_to_vn(self, telex_syllable):
        c, r, t, done = self.telex_to_consonant_rhyme_tone(telex_syllable)
        return self.add_consonant_rhyme_tone(c, r, t)

    # Chua kiem tra co dung la telex syllable hay ko ?
    def telex_to_consonant_rhyme_tone(self, syllable):
        syllable_len = len(syllable)
        if syllable_len == 0:
            return None, None, None, False

        # Find tone
        last_char = syllable[-1]
        tone, ok = self.tone_helper.tone_char2tone(last_char)
        if not ok:
            tone = 0

        if tone > 0:
            syllable = syllable[:syllable_len - 1]

        syllable_len = len(syllable)
        addition_start_index = syllable_len - 1
        current_index = 0

        # Duyet nguoc tu duoi len tim vi tri bat dau cua addition
        for i in reversed(range(syllable_len)):
            if syllable[i] == u'd':
                addition_start_index = i
                break
            elif syllable[i] == u'w':
                pass
            elif syllable[i] in u'aeo':
                is_done = False
                while True:
                    if current_index == i:
                        addition_start_index = i + 1
                        is_done = True
                        break
                    if syllable[current_index] == syllable[i]:
                        # ki tu vi tri i thuoc addition
                        if current_index == i - 1:
                            addition_start_index = i
                            is_done = True
                        break
                    else:
                        current_index += 1
                # Hoan thanh break vong for
                if is_done:
                    break
            else:
                addition_start_index = i + 1
                break

        addition = ''
        if addition_start_index == 0:
            base = syllable
        else:
            base = syllable[0:addition_start_index]
            addition = syllable[addition_start_index:]

        if len(addition) > 0 and addition[0] == 'd':
            addition = addition[1:]
            base = list(base)
            base[0] = u'đ'
            base = ''.join(base)

        start_base_rhyme_index = 0

        base_len = len(base)
        for i in range(base_len):
            if base[i] in 'aeiouy':
                start_base_rhyme_index = i
                break
        consonant = base[:start_base_rhyme_index]
        base_rhyme = base[start_base_rhyme_index:]

        if consonant == u'g':
            if len(base_rhyme) >= 2:
                if base_rhyme[0] == u'i':
                    if base_rhyme[1] in 'aeiouy':
                        base_rhyme = base_rhyme[1:]
                        consonant += 'i'
                    else:
                        consonant += 'i'

        if consonant == u'q':
            if len(base_rhyme) >= 2:
                if base_rhyme[0] == u'u' and base_rhyme[1] in u'ae':
                    base_rhyme = list(base_rhyme)
                    base_rhyme[0] = u'o'
                    base_rhyme = ''.join(base_rhyme)

        # Truong hop 'o' o cuoi
        if len(addition) > 0 and addition[0] == u'o':
            if self.rhyme_helper.is_rhyme(base_rhyme + 'o'):
                base_rhyme += 'o'
                addition = addition[1:]

        rhyme, done = self.rhyme_helper.add_base_rhyme_addition(base_rhyme, addition)
        if done:
            # giữ gìn -> gi ư x, gi in f
            return consonant, rhyme, tone, True

        return None, None, None, False



        # def correct_syllable(self, incorrect_syllable):
        #     telex = self.any_to_telex(incorrect_syllable)
        #     print(telex)
        #     if self.is_telex_syllable(telex):
        #         return None, False
        #     return self.telex_to_vn(telex), True


if __name__ == "__main__":
    from lang.Tone import ToneHelper
    from lang.Char import CharHelper
    from lang.Consonant import ConsonantHelper
    from lang.Rhyme import RhymeHelper

    tone_helper = ToneHelper()
    char_helper = CharHelper(tone_helper=tone_helper)
    rhyme_helper = RhymeHelper(tone_helper=tone_helper, char_helper=char_helper)
    consonant_helper = ConsonantHelper()

    syllable_helper = SyllableHelper(tone_helper=tone_helper,
                                     char_helper=char_helper,
                                     consonant_helper=consonant_helper,
                                     rhyme_helper=rhyme_helper)

    syllables = syllable_helper.find_all_vn_syllables()
    print(len(syllables))
